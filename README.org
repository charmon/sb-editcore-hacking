
* SBCL Editcore Hacking

Douglas Katzman has recently added some machinery to SBCL such that
one can build a statically built ELF executable containing SBCL and
what would usually be in the core file loaded by SBCL. This would
allow for executable binaries to be packaged up as a single binary
file and seems to provide very rapid startup times.

* Trivial Example

The canonical example to make this work is:

#+BEGIN_SRC sh :results output
./run-sbcl.sh --eval "(save-lisp-and-die \"output/foo.core\")"

cd src/runtime

../../run-sbcl.sh --script ../../tools-for-build/editcore.lisp split \
                  ../../output/foo.core shrinkwrap-sbcl.s

make shrinkwrap-sbcl

./shrinkwrap-sbcl
#+END_SRC

* CLIM

I'm trying to make this work with a core that has McCLIM and the
clim-examples loaded into it:

#+BEGIN_SRC sh :results output
./run-sbcl.sh --eval "(asdf:load-system \"clim-examples\")" \
              --eval "(save-lisp-and-die \"output/clim.core\")"

cd src/runtime

../../run-sbcl.sh --script ../../tools-for-build/editcore.lisp split \
                  ../../output/clim.core shrinkwrap-sbcl.s
#+END_SRC

But  there's a  problem where  sb-editcore::fun-name-from-core gets  a
symbol named "" and isn't happy. This code exists  to help track down
the problem.

