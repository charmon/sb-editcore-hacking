
(in-package "SB-EDITCORE-HACKING")

(sb-vm::map-allocated-objects
 (lambda (obj type size)
   (when (and (= type sb-vm:code-header-widetag) 
              (zerop (length (fun-name-from-core obj nil nil nil))))
     (print obj)))
 :all)

